package com.example

import com.example.model.StreamForCVData
import java.util.*

class CVController {

    private val runningCVModels = mutableMapOf<String, Long>()
    private val reservedPorts = mutableListOf<String>().apply {
        for (port in 5555..5580) {
            add("$port")
        }
    }

    suspend fun runCV(cvData: StreamForCVData) {
        val port = reservedPorts.removeLast()
        val localUUID = UUID.randomUUID().toString()
        val localRtspUrl = "rtsp://127.0.0.1:$port/$localUUID"
        val remoteUrl = convertRtspToRtmp(cvData.streamSource)
        val debugInfoCvModel = "${port}_${localUUID}_${cvData.cvModel}.txt"
        val ffmpegPushDebugInfo = "${port}_${localUUID}_ffmpeg.txt"
        val cvCommand = "~/Diploma/AI/source/jetson-inference/build/aarch64/bin/${cvData.cvModel} " +
                "--input-codec=h264 " +
                "${cvData.streamSource} " +
                " $localRtspUrl  > $debugInfoCvModel ; rm $debugInfoCvModel"
        val pushBackToStreamingServer =
            "ffmpeg -i $localRtspUrl -c:v copy -f flv $remoteUrl > $ffmpegPushDebugInfo ; rm $ffmpegPushDebugInfo "

        val command = "$cvCommand && sleep 60 && $pushBackToStreamingServer"
        println(command)
        runCommand(command, false) {
            when (it) {
                is ProcessResult.Error -> reservedPorts.add(port)
                is ProcessResult.Running -> runningCVModels[cvData.streamSource] = it.pid
                ProcessResult.Success -> reservedPorts.add(port)
            }
        }
    }

    fun stopCV(cvData: StreamForCVData) {
        val pid = runningCVModels[cvData.streamSource] ?: return
        val processHandle = ProcessHandle.of(pid)
        if (processHandle.isPresent) {
            processHandle.get().destroy()
        }
        runningCVModels.remove(cvData.streamSource)
    }

    private fun convertRtspToRtmp(rtspUrl: String): String {
        val ipAddressRegex = "(?<=rtsp://)[^:/]+".toRegex()
        val streamKeyRegex = "(?<=/)[^/]+\$".toRegex()

        val ipAddressMatchResult = ipAddressRegex.find(rtspUrl)
        val streamKeyMatchResult = streamKeyRegex.find(rtspUrl)

        val ipAddress = ipAddressMatchResult?.value
        val streamKey = streamKeyMatchResult?.value

        return if (ipAddress != null && streamKey != null) {
            "rtmp://$ipAddress:1935/cv/$streamKey"
        } else {
            throw IllegalArgumentException("Invalid RTSP URL format")
        }
    }


}