package com.example

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

suspend fun runCommand(
    command: String,
    waiteForResult: Boolean = true,
    onResult: (ProcessResult) -> Unit = { }
) {
    val proc: Process = try {
        withContext(Dispatchers.IO) {
            Runtime.getRuntime().exec(command)
        }
    } catch (e: IOException) {
        onResult(ProcessResult.Error(error = e))
        return
    }
    onResult(ProcessResult.Running(proc.pid()))

    if (waiteForResult) {
        val exitCode = try {
            withContext(Dispatchers.IO) {
                proc.waitFor()
            }
            proc.exitValue()
        } catch (e: InterruptedException) {
            onResult(ProcessResult.Error(error = e))
            return
        }

        if (exitCode != 0) {
            onResult(ProcessResult.Success)
        } else {
            onResult(ProcessResult.Error(exitCode))
        }
    }
}


fun runCommandBlocking(
    command: String,
    waiteForResult: Boolean = true,
    onResult: (ProcessResult) -> Unit = { }
) {
    val proc: Process = try {
        Runtime.getRuntime().exec(command)
    } catch (e: IOException) {
        onResult(ProcessResult.Error(error = e))
        return
    }
    onResult(ProcessResult.Running(proc.pid()))

    if (waiteForResult) {
        val exitCode = try {
            proc.waitFor()
            proc.exitValue()
        } catch (e: InterruptedException) {
            onResult(ProcessResult.Error(error = e))
            return
        }

        if (exitCode != 0) {
            onResult(ProcessResult.Success)
        } else {
            onResult(ProcessResult.Error(exitCode))
        }
    }
}


sealed interface ProcessResult {
    object Success : ProcessResult
    class Running(val pid: Long) : ProcessResult
    class Error(val errorCode: Int = 0, val error: Throwable? = null) : ProcessResult
}