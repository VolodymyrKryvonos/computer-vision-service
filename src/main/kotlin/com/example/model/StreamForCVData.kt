package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class StreamForCVData(
    val streamSource: String,
    val cvModel: String
)
