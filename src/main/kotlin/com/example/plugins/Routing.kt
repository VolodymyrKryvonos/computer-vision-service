package com.example.plugins

import com.example.CVController
import com.example.model.StreamForCVData
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    val controller = CVController()
    routing {
        post("/startCV") {
            val cvData = this.call.receiveNullable<StreamForCVData>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }
            controller.runCV(cvData)
            call.respond(HttpStatusCode.OK)
        }

        post("/stopCV") {
            val cvData = this.call.receiveNullable<StreamForCVData>() ?: run {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }
            controller.stopCV(cvData)
            call.respond(HttpStatusCode.OK)
        }
    }
}
